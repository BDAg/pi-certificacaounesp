-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 22/05/2019 às 01:26
-- Versão do servidor: 5.6.43
-- Versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `cenarior_ceps`
--
CREATE DATABASE IF NOT EXISTS `cenarior_ceps` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cenarior_ceps`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `alternativas`
--

DROP TABLE IF EXISTS `alternativas`;
CREATE TABLE `alternativas` (
  `id_alternativa` int(11) NOT NULL,
  `alternativa` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `formularios`
--

DROP TABLE IF EXISTS `formularios`;
CREATE TABLE `formularios` (
  `id_formulario` int(11) NOT NULL,
  `data_formulario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `proprietarioID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `formularios_respostas`
--

DROP TABLE IF EXISTS `formularios_respostas`;
CREATE TABLE `formularios_respostas` (
  `id` int(11) NOT NULL,
  `formularioID` int(11) DEFAULT NULL,
  `respostasID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `perguntas`
--

DROP TABLE IF EXISTS `perguntas`;
CREATE TABLE `perguntas` (
  `id_pergunta` int(11) NOT NULL,
  `pergunta` varchar(250) DEFAULT NULL,
  `tipoproducaoID` int(11) DEFAULT NULL,
  `tipoperguntasID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `perguntas_alternativas`
--

DROP TABLE IF EXISTS `perguntas_alternativas`;
CREATE TABLE `perguntas_alternativas` (
  `perguntaID` int(11) DEFAULT NULL,
  `alternativaID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `producoes`
--

DROP TABLE IF EXISTS `producoes`;
CREATE TABLE `producoes` (
  `id_producoes` int(11) NOT NULL,
  `descricao_producao` varchar(45) DEFAULT NULL,
  `proprietariosID` int(11) DEFAULT NULL,
  `tiposproducaoID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `proprietarios`
--

DROP TABLE IF EXISTS `proprietarios`;
CREATE TABLE `proprietarios` (
  `id_proprietario` int(11) NOT NULL,
  `nome_proprietario` varchar(100) NOT NULL,
  `email_proprietario` varchar(100) NOT NULL,
  `senha_proprietario` varchar(129) NOT NULL,
  `telefone_fixo` varchar(30) DEFAULT NULL,
  `celular` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `respostas`
--

DROP TABLE IF EXISTS `respostas`;
CREATE TABLE `respostas` (
  `id_resposta` int(11) NOT NULL,
  `disertativa_resposta` varchar(300) DEFAULT NULL,
  `alternativaID` int(11) DEFAULT NULL,
  `perguntasID` int(11) DEFAULT NULL,
  `formID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipos_perguntas`
--

DROP TABLE IF EXISTS `tipos_perguntas`;
CREATE TABLE `tipos_perguntas` (
  `id_tipo_pergunta` int(11) NOT NULL,
  `descricao_tipo_pergunta` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipos_producao`
--

DROP TABLE IF EXISTS `tipos_producao`;
CREATE TABLE `tipos_producao` (
  `id_tipo_producao` int(11) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `alternativas`
--
ALTER TABLE `alternativas`
  ADD PRIMARY KEY (`id_alternativa`);

--
-- Índices de tabela `formularios`
--
ALTER TABLE `formularios`
  ADD PRIMARY KEY (`id_formulario`),
  ADD KEY `proprietarioID` (`proprietarioID`);

--
-- Índices de tabela `formularios_respostas`
--
ALTER TABLE `formularios_respostas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formularioID` (`formularioID`),
  ADD KEY `respostasID` (`respostasID`);

--
-- Índices de tabela `perguntas`
--
ALTER TABLE `perguntas`
  ADD PRIMARY KEY (`id_pergunta`),
  ADD KEY `tipoproducaoID` (`tipoproducaoID`),
  ADD KEY `tipoperguntasID` (`tipoperguntasID`);

--
-- Índices de tabela `perguntas_alternativas`
--
ALTER TABLE `perguntas_alternativas`
  ADD KEY `perguntaID` (`perguntaID`),
  ADD KEY `alternativaID` (`alternativaID`);

--
-- Índices de tabela `producoes`
--
ALTER TABLE `producoes`
  ADD PRIMARY KEY (`id_producoes`),
  ADD KEY `proprietariosID` (`proprietariosID`),
  ADD KEY `tiposproducaoID` (`tiposproducaoID`);

--
-- Índices de tabela `proprietarios`
--
ALTER TABLE `proprietarios`
  ADD PRIMARY KEY (`id_proprietario`);

--
-- Índices de tabela `respostas`
--
ALTER TABLE `respostas`
  ADD PRIMARY KEY (`id_resposta`),
  ADD KEY `perguntasID` (`perguntasID`);

--
-- Índices de tabela `tipos_perguntas`
--
ALTER TABLE `tipos_perguntas`
  ADD PRIMARY KEY (`id_tipo_pergunta`);

--
-- Índices de tabela `tipos_producao`
--
ALTER TABLE `tipos_producao`
  ADD PRIMARY KEY (`id_tipo_producao`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `alternativas`
--
ALTER TABLE `alternativas`
  MODIFY `id_alternativa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `formularios`
--
ALTER TABLE `formularios`
  MODIFY `id_formulario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `formularios_respostas`
--
ALTER TABLE `formularios_respostas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `perguntas`
--
ALTER TABLE `perguntas`
  MODIFY `id_pergunta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `producoes`
--
ALTER TABLE `producoes`
  MODIFY `id_producoes` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `proprietarios`
--
ALTER TABLE `proprietarios`
  MODIFY `id_proprietario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `respostas`
--
ALTER TABLE `respostas`
  MODIFY `id_resposta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tipos_perguntas`
--
ALTER TABLE `tipos_perguntas`
  MODIFY `id_tipo_pergunta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tipos_producao`
--
ALTER TABLE `tipos_producao`
  MODIFY `id_tipo_producao` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
