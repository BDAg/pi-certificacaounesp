drop database ceps;
create database ceps;
use ceps;
create table if not exists proprietarios (
    id_proprietario int not null primary key auto_increment,
    nome_proprietario varchar(100),
    email_proprietario varchar(100),
    senha_proprietario varchar(129),
);

create table if not exists formularios(
	id_formulario int not null primary key auto_increment,
    data_formulario date,
    proprietarioID int,
    foreign key (proprietarioID) references proprietarios(id_proprietario)
);

create table if not exists tipos_producao(
	id_tipo_producao int not null primary key auto_increment,
    descricao varchar(45)
);

create table if not exists producoes(
	id_producoes int not null primary key auto_increment,
    descricao_producao varchar(45),
    proprietariosID int,
    foreign key (proprietariosID) references proprietarios(id_proprietario),
    tiposproducaoID int,
    foreign key (tiposproducaoID) references tipos_producao(id_tipo_producao)
);

create table if not exists tipos_perguntas(
	id_tipo_pergunta int not null primary key auto_increment,
    descricao_tipo_pergunta varchar(45)
);

create table if not exists perguntas(
	id_pergunta int not null primary key auto_increment,
    pergunta varchar(100),
    tipoproducaoID int,
    foreign key (tipoproducaoID) references tipos_producao(id_tipo_producao),
	tipoperguntasID int,
    foreign key (tipoperguntasID) references tipos_perguntas(id_tipo_pergunta)
);

create table if not exists respostas(
	id_resposta int not null primary key auto_increment,
	disertativa_resposta varchar(300),
    perguntasID int,
    foreign key (perguntasID) references perguntas(id_pergunta)
);

create table if not exists formularios_respostas(
	formularioID int,
    foreign key (formularioID) references formularios(id_formulario),
    respostasID int,
    foreign key (respostasID) references respostas(id_resposta)
);

create table if not exists alternativas(
	id_alternativa int not null primary key auto_increment,
    alternativa varchar(200)
);

create table if not exists respostas_alternativas(
	respostaID int,
    foreign key (respostaID) references respostas(id_resposta),
    alternativaID int,
    foreign key (alternativaID) references alternativas(id_alternativa)
);

create table if not exists perguntas_alternativas(
	perguntaID int,
    foreign key (perguntaID) references perguntas(id_pergunta),
    alternativaID int,
    foreign key (alternativaID) references alternativas(id_alternativa)
);
