import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalProvider } from "../global/global";

/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {

  basepath = '/api';

  constructor(public http: HttpClient,
    public global: GlobalProvider) { }


  login(form): Promise<any> {

    let body = {
      email: form.email,
      senha: form.senha
    };

    return new Promise((resolve, reject) => {
      this.http.post(`${this.basepath}/usuario/login`, body, { headers: this.global.getHeaders(), observe: 'response' })
        .toPromise()
        .then(data => {
          if (data.status == 200) {
            resolve(data.body);
          } else {
            resolve({});
          }
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  cadastro(form): Promise<any> {
    let body = {
      nome: form.nome,
      email: form.email,
      senha: form.senha,
      telefone: form.telefone,
      celular: form.celular
    };

    return new Promise((resolve, reject) => {
      this.http.post(`${this.basepath}/usuario/cadastro`, body, { headers: this.global.getHeaders(), observe: 'response' })
        .toPromise()
        .then(data => {
          if (data.status == 200) {
            resolve(data.body);
          } else {
            resolve({});
          }
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  // edita(): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     this.http.post(`${this.basepath}/usuario/editar/`, { headers: this.global.getHeaders(), observe: 'response' })
  //       .toPromise()
  //       .then(data => {
  //         if (data.status == 200) {
  //           resolve(data.body);
  //         } else {
  //           resolve({});
  //         }
  //       })
  //       .catch(err => {
  //         reject(err);
  //       });
  //   })
  // }
}
