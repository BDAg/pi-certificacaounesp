import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {GlobalProvider} from "../global/global";

/*
  Generated class for the FormProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FormProvider {

  basepath = "/api";

  constructor(public http: HttpClient,
              public global: GlobalProvider) {
  }

  getGeneralQuestions():Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.basepath}/form/questions/general`, {headers: this.global.getHeaders(), observe: 'response'})
        .toPromise()
        .then(data => {
          if (data.status == 200) {
            resolve(data.body);
          } else {
            resolve({});
          }
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  getBovCorQuestions():Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.basepath}/form/questions/bovino_corte`, {headers: this.global.getHeaders(), observe: 'response'})
        .toPromise()
        .then(data => {
          if (data.status == 200) {
            resolve(data.body);
          } else {
            resolve({});
          }
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  getBovLeiQuestions():Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.basepath}/form/questions/bovino_leite`, {headers: this.global.getHeaders(), observe: 'response'})
        .toPromise()
        .then(data => {
          if (data.status == 200) {
            resolve(data.body);
          } else {
            resolve({});
          }
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  getAvePosQuestions():Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.basepath}/form/questions/ave_postura`, {headers: this.global.getHeaders(), observe: 'response'})
        .toPromise()
        .then(data => {
          if (data.status == 200) {
            resolve(data.body);
          } else {
            resolve({});
          }
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  getLastForm(idProp): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.basepath}/form/info/lastform`, idProp, {headers: this.global.getHeaders(), observe: 'response'})
        .toPromise()
        .then(data => {
          if (data.status == 200) {
            resolve(data.body);
          } else {
            resolve({});
          }
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  postsaveForm(respostas): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.basepath}/form/save`, respostas, {headers: this.global.getHeaders(), observe: 'response'})
        .toPromise()
        .then(data => {
          if (data.status == 200) {
            resolve(data.body);
          } else {
            resolve({});
          }
        })
        .catch(err => {
          reject(err);
        });
    })
  }

  getProgress(idProp): Promise<any> {

    return new Promise((resolve, reject) => {
      this.http.post(`${this.basepath}/form/info/progress`, idProp, {headers: this.global.getHeaders(), observe: 'response'})
        .toPromise()
        .then(data => {
          if(data.status == 200) {
            resolve(data.body);
          }else{
            resolve(null)
          }
        })
        .catch(err => {
          reject(err);
        })
    })

  }

}
