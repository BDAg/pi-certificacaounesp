import {Injectable} from '@angular/core';
import {ToastController} from "ionic-angular";

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {


  constructor(private toastCrtl: ToastController) {
  }

  public user: {};
  public headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
  };

  public basepath = 'http://localhost:3000';

  public setGlobalUser(user: {}) {
    this.user = user;
  }

  public getGlobalUser() {
    return this.user;
  }

  public getBasePath() {
    return this.basepath;
  }

  public getHeaders() {
    return this.headers;
  }

  public createToast(Message) {
    this.toastCrtl.create({
      message: Message,
      duration: 2000
    }).present();
  }

}
