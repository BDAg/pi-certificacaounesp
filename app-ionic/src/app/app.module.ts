import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { DashboardPage } from "../pages/dashboard/dashboard";
import { LoginPage } from '../pages/login/login';
import { CadastroPage } from '../pages/cadastro/cadastro';
import { QueGeralPage } from '../pages/que-geral/que-geral';
import { PerfilPage } from '../pages/perfil/perfil';

import { UsuarioProvider } from '../providers/usuario/usuario';
import { GlobalProvider } from '../providers/global/global';
import { HttpClientModule } from "@angular/common/http";
import { FormProvider } from '../providers/form/form';
import { NativeStorage } from "@ionic-native/native-storage";


@NgModule({
  declarations: [
    MyApp,
    DashboardPage,
    LoginPage,
    CadastroPage,
    QueGeralPage,
    PerfilPage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashboardPage,
    LoginPage,
    CadastroPage,
    QueGeralPage,
    PerfilPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UsuarioProvider,
    GlobalProvider,
    FormProvider,
    NativeStorage
  ]
})
export class AppModule {
}
