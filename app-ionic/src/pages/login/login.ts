import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';
import { UsuarioProvider } from "../../providers/usuario/usuario";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DashboardPage } from "../dashboard/dashboard";
import { GlobalProvider } from "../../providers/global/global";
import { NativeStorage } from "@ionic-native/native-storage";
import { CadastroPage } from "../cadastro/cadastro";


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {


  form: FormGroup;

  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public userProv: UsuarioProvider,
    public global: GlobalProvider,
    public storage: NativeStorage,
    public loadCtrl: LoadingController) {

    this.form = formBuilder.group({
      email: ['', Validators.email],
      senha: ['', Validators.required]
    });

  }


  ngOnInit() {
    const load = this.loadCtrl.create({
      content: "Aguarde"
    });
    load.present();


    this.storage.getItem("user").then(
      data => {
        load.setDuration(250);
        this.global.setGlobalUser(data);
        this.navCtrl.setRoot(DashboardPage);
      },
      error => {
        load.setDuration(250);
      });



  }

  async SingIn() {

    if (this.form.status == "VALID") {
      let data = await this.userProv.login(this.form.value).then(v => {
        return v;
      });


      if (JSON.stringify(data) != "{}") {
        this.navCtrl.setRoot(DashboardPage);
        this.storage.setItem("user", data).then(
          () => console.log("Usuario Salvo")
        );

        this.navCtrl.setRoot(DashboardPage);

        this.global.setGlobalUser(data);
      } else {
        this.global.createToast("Login Invalido");
      }

    }

  }

  SingUp() {
    this.navCtrl.push(CadastroPage);
  }

}

