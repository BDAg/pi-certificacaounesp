import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GlobalProvider } from '../../providers/global/global';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { NativeStorage } from '@ionic-native/native-storage';



@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  form: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public global: GlobalProvider,
    public storage: NativeStorage,
    public userProv: UsuarioProvider) {

    this.form = formBuilder.group({
      nome: ['', Validators.required],
      telefone: [''],
      celular: ['']
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  // async editarPerfil() {
  //   if (this.form.status == "VALID") {
  //     let data = this.userProv.editar(this.form.value).then(v => {
  //       return v;
  //     });
  //     if (JSON.stringify(data) != "{}") {
  //       this.navCtrl.setRoot(DashboardPage);
  //       this.storage.setItem("user", data).then(
  //         () => console.log("Usuario Salvo")
  //       );
  //     }
  //   }
  // }

}
