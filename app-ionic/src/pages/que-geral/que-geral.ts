import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {FormProvider} from "../../providers/form/form";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {GlobalProvider} from "../../providers/global/global";
import {DashboardPage} from "../dashboard/dashboard";

/**
 * Generated class for the QueGeralPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-que-geral',
  templateUrl: 'que-geral.html',
})
export class QueGeralPage {

  questoes: any = [];
  payload: any;
  form: FormGroup;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formProv: FormProvider,
              public formBuilder: FormBuilder,
              public global:GlobalProvider) {

    this.payload = this.navParams.get("payload");
    this.form = new FormGroup({});
    this.loaderForm();
  }


  async loaderForm() {

    await this.carregarQuestoes();

    for (let questao of this.questoes) {

      if (questao["tipo"] == "radio") {
        this.form.addControl(questao["tipo"] + questao["ID"], new FormControl('',Validators.required));
      } else if (questao["tipo"] == "checkbox") {
        for (let alternativa of questao["alternativas"]) {
          this.form.addControl(questao["tipo"] + questao["ID"] + "|" + alternativa["ID"], new FormControl(false,Validators.required));
        }
      } else {
        this.form.addControl("input" + questao["ID"], new FormControl('',Validators.required));
      }

    }


  }

  async carregarQuestoes() {
    if (this.payload.idStep == 0) {
      this.questoes = await this.formProv.getGeneralQuestions().then(v => {
        return v;
      });
    } else if (this.payload.idStep == 1) {
      this.questoes = await this.formProv.getBovCorQuestions().then(v => {
        return v;
      });
    } else if (this.payload.idStep == 2) {
      this.questoes = await this.formProv.getBovLeiQuestions().then(v => {
        return v;
      });
    } else if (this.payload.idStep == 3) {
      this.questoes = await this.formProv.getAvePosQuestions().then(v => {
        return v;
      });
    }
  }


  proximoQuestionario() {

    if(this.form.valid) {
      if (this.form.get("radio3").value == 8) {
        this.navCtrl.push(QueGeralPage, {
          "payload":
            {
              title: "Questionário Bovino de Corte",
              idStep: 1,
              dados: this.form.value
            }
        });
      } else if (this.form.get("radio3").value == 9) {
        this.navCtrl.push(QueGeralPage, {
          "payload":
            {
              title: "Questionário Bovino para Leite",
              idStep: 2,
              dados: this.form.value
            }
        });
      } else if (this.form.get("radio3").value == 10) {
        this.navCtrl.push(QueGeralPage, {
          "payload":
            {
              title: "Questionário Aves de Postura",
              idStep: 3,
              dados: this.form.value
            }
        });
      }
    }else{
      this.global.createToast("Responda todas as questões!")
    }
  }


  fimQuestionario(){

    if(this.form.valid) {
      var respostas = [];

      Object.keys(this.payload.dados).forEach(a => {
        if (a.includes("radio")) {
          respostas.push({
            idPergunta: a.replace("radio", ''),
            idAlternativa: this.payload.dados[a]
          });
        } else if (a.includes("checkbox")) {
          if (this.payload.dados[a] == true) {
            respostas.push({
              idPergunta: a.replace("checkbox", '').split("|", 2)[0],
              idAlternativa: a.replace("checkbox", '').split("|", 2)[1]
            });
          }
        }
      });

      for (let v in this.form.value) {
        if (v.includes("radio")) {
          respostas.push({
            idPergunta: v.replace("radio", ''),
            idAlternativa: this.form.get(v).value
          });
        } else if (v.includes("checkbox")) {
          if (this.form.get(v).value == true) {
            respostas.push({
              idPergunta: v.replace("checkbox", '').split("|", 2)[0],
              idAlternativa: v.replace("checkbox", '').split("|", 2)[1]
            });
          }
        }
      }

      // console.log(this.form.value);
      // console.log(this.payload.dados);
      // console.log("Questionario Finalizado");
      // console.log(respostas);


      this.salvar(respostas)

    }else{

      this.global.createToast("Responda todas as questões!")

    }

  }

  async salvar(respostas){

    await this.formProv.postsaveForm({
      idProprietario: this.global.user["id_proprietario"],
      respostas:respostas
    }).then();

      this.global.createToast("Questionário Salvo com Sucesso !");
      this.navCtrl.setRoot(DashboardPage);
  }

}
