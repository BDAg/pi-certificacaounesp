import {Component, OnInit} from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import { GlobalProvider } from "../../providers/global/global";
import { NativeStorage } from "@ionic-native/native-storage";
import { LoginPage } from "../login/login";
import { PerfilPage } from "../perfil/perfil";
import { QueGeralPage } from "../que-geral/que-geral";

import * as echart from "../../assets/js/echarts.min.js";
import {FormProvider} from "../../providers/form/form";


/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage implements OnInit{

  usuario: any;
  load = this.loadCtrl.create({
    content:'Aguarde'
  });

  ShowProgress = false;


  constructor(
    public navCtrl: NavController,
    public global: GlobalProvider,
    public navParams: NavParams,
    private storage: NativeStorage,
    public loadCtrl: LoadingController,
    public formProv:FormProvider) {

    this.usuario = this.global.getGlobalUser();
  }

  exit() {
    this.global.setGlobalUser({});
    this.storage.remove("user").then(
      () => console.log("Sessão Destruida")
    );
    this.navCtrl.setRoot(LoginPage);
  }

  perfil() {
    this.navCtrl.push(PerfilPage);
  }

  questoesGerais() {
    this.navCtrl.push(QueGeralPage,{"payload":{title:"Questionário Geral",idStep:0}});
  }



  ngOnInit(){

    this.load.present();

    // this.createPie();
    this.loader();


  }

  async loader(){
    if(this.lastForm() != {}){
      this.getProgress();
    }
    this.load.setDuration(200);

  }

  async lastForm(){

    await this.formProv.getLastForm({idProp: this.global.user["id_proprietario"]}).then(value => {
      return value;
    });

  }

  async getProgress(){
    await this.formProv.getProgress({idProp: this.global.user["id_proprietario"]}).then(value => {
      // console.log(value);
      if(value != null) {
        this.ShowProgress = true;
        document.getElementById("grafico").hidden;
        this.createPie(value['respondido'], (value['total']-value['respondido']));
      }
    })
  }

  createPie(respondido,total){
    let pizza = echart.init(document.getElementById("grafico"));
    let option = {
      series: [
        {
          type: 'pie',
          radius: ['55%', '70%'],
          label: {
            normal: {
              show: true,
              position: 'center',
              formatter: "{b}\n{d}%",
              backgroundColor: "white",
              fontSize: "20",
              fontWeight: 'bold'
            },
            emphasis: {
              show: true,
              textStyle: {
                fontSize: '24',
                fontWeight: 'bold'
              }
            }
          },
          data: [
            {value: total, name: 'Restante'},
            {value: respondido, name: 'Concluido'}
          ]
        }
      ],
      color: ["cyan", "lightgreen"],
      backgroundColor: "white"
    };

    pizza.setOption(option);
  }

}
