import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {GlobalProvider} from "../../providers/global/global";
import {UsuarioProvider} from "../../providers/usuario/usuario";
import {LoginPage} from "../login/login";
import {NativeStorage} from "@ionic-native/native-storage";


@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  form: FormGroup;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public userProv: UsuarioProvider,
    public global: GlobalProvider,
    public storage: NativeStorage,) {

    this.form = formBuilder.group({
      nome: ['', Validators.required],
      telefone: [''],
      celular: [''],
      email: ['', Validators.email],
      senha: ['', Validators.required],
      confSenha: ['', Validators.required],
    })

  }

  async cadastrar() {


    if (this.form.status == "VALID") {

      if (this.form.get("senha").value != this.form.get("confSenha").value) {
        this.form.controls["senha"].setValue("");
        this.form.controls["confSenha"].setValue("");
        this.global.createToast("As senhas não são iguais")
      } else {

        let data = this.userProv.cadastro(this.form.value).then(v => {
          return v;
        });
        if (JSON.stringify(data) != "{}") {
          this.navCtrl.setRoot(LoginPage);
        }
      }

    }
  }

}
