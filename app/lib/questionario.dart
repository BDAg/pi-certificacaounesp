import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class Question_Screen extends StatefulWidget {
  @override
  _Question_State createState() => _Question_State();
}

class _Question_State extends State<Question_Screen> {
  List<String> _locations = ['Tupã', 'Marilia', 'Bastos']; // Option 2
  String _selectedLocation; // Option 2
  int grupoValor;

  @override
  Widget build(BuildContext ctxt) {
    return Scaffold(
      appBar: AppBar(title: Text('Projeto CePS')),
      body: Center(
          child: SingleChildScrollView(
        child: Container(
            width: 450,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(20.0),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Qual o tamanho da sua propriedade ?',
                        prefixIcon: Icon(Icons.home),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(20.0),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Qual o tamanho do rebanho ?',
                        prefixIcon: Icon(Icons.blur_on),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(15.0),
                    child: DropdownButton(
                      hint: Text('Em Qual cidade a propriedade se localiza?'),
                      // Not necessary for Option 1
                      value: _selectedLocation,
                      onChanged: (newvalue) {
                        setState(() {
                          _selectedLocation = newvalue;
                        });
                      },
                      items: _locations.map((location) {
                        return DropdownMenuItem(
                          child: new Text(location),
                          value: location,
                        );
                      }).toList(),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.all(50.0),
                      child: Column(
                        children: <Widget>[
                          Text('Qual sua criação animal?', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                          Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  new Radio(
                                    onChanged: (int e) => something(e),
                                    value: 1,
                                    groupValue: grupoValor,
                                  ),
                                  new Text('Galinha de Postura'), 
                                ],
                              ),

                              Row(
                                children: <Widget>[
                                  new Radio(
                                    onChanged: (int e) => something(e),
                                    value: 2,
                                    groupValue: grupoValor,
                                  ),
                                  new Text('Bovino de Corte'), 
                                ],
                              ),

                              Row(
                                children: <Widget>[
                                  new Radio(
                                    onChanged: (int e) => something(e),
                                    value: 3,
                                    groupValue: grupoValor,
                                  ),
                                  new Text('Bovino de Leite'), 
                                ],
                              ),
                            ],
                          ),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Column(
                        children: <Widget>[
                          Text('Esta é sua atividade economica principal?', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                          Row(
                            children: <Widget>[
                              new Radio( ),
                              new Text('Sim'),
                              new Radio( value: 0,),
                              new Text('Não'),
                            ],
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: RaisedButton(
                      onPressed: () {
//                        Navigator.push(
//                            ctxt,
//                            new MaterialPageRoute(builder: (ctxt) => new SignUp_Screen()));
                      },
                      child: const Text(
                        'Cadastrar-se',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ])),
      )),
    );
  }

  void something(int e) {
    setState(() {
      if (e == 1) {
        grupoValor = 1;
      } else if (e == 2){
        grupoValor = 2;
      } else if (e == 3){
        grupoValor = 3;
      }
    });
  }
}
