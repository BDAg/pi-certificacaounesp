import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class SignUp_Screen extends StatelessWidget {
  @override
  Widget build(BuildContext ctxt) {
    return Scaffold(
      appBar: AppBar(title: Text('Projeto CePS')),
      body: Center(
          child: SingleChildScrollView(
        child: Container(
            width: 400,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Digite seu Nome Completo',
                    prefixIcon: Icon(Icons.person),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Digite seu Email',
                    prefixIcon: Icon(Icons.email),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Digite seu Nome de Usuário',
                    prefixIcon: Icon(Icons.person),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Digite sua Senha',
                    prefixIcon: Icon(Icons.vpn_key),
                  ),
                  obscureText: true,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Confirmar sua Senha',
                    prefixIcon: Icon(Icons.vpn_key),
                  ),
                  obscureText: true,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: RaisedButton(
                  onPressed: () {
//                        Navigator.push(
//                            ctxt,
//                            new MaterialPageRoute(builder: (ctxt) => new SignUp_Screen()));
                  },
                  child: const Text(
                    'Cadastrar-se',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ])),
      )),
    );
  }
}