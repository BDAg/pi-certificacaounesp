const db = require("../db");

exports.lastform = function (req, res) {

    id = req.body["idProp"];

    db.getConnection(function (err, conn) {
        conn.query("select * from formularios where proprietarioID = ? order by data_formulario DESC LIMIT 1;", [id], (error, result) => {
            if (result.length === 0) {
                res.status(204).send({msg: "Não a registros"});
                conn.release();
            } else {
                res.status(200).send(result[0]);
                conn.release();
            }
        });
    });

};

exports.generalQuestions = function (req, res) {

    db.getConnection(function (err, conn) {
        conn.query("select a.alternativa, pa.alternativaID, p.pergunta, p.id_pergunta, p.tipoperguntasID from perguntas_alternativas as pa inner join perguntas as p on p.id_pergunta = pa.perguntaID inner join alternativas as a on a.id_alternativa = pa.alternativaID where p.tipoproducaoID is null;"
            , (error, result) => {
                if (result.length === 0) {
                    res.status(204).send({msg: "Não a registros"});
                    conn.release();
                } else {

                    var questoes = [];

                    // alternativa
                    // pergunta
                    // id_pergunta

                    // Adiciona todas as perguntas;
                    Object.values(result).forEach(value => {
                        if (questoes[value.id_pergunta] == null) {
                            questoes[value.id_pergunta] = {};
                            questoes[value.id_pergunta]["pergunta"] = value.pergunta;
                            questoes[value.id_pergunta]["alternativas"] = [];
                            // questoes[value.id_pergunta]["id"] = value.id_pergunta;
                            if (value.tipoperguntasID == 1) {
                                questoes[value.id_pergunta]["tipo"] = "radio";
                            } else if (value.tipoperguntasID == 2) {
                                questoes[value.id_pergunta]["tipo"] = "input";
                            } else if (value.tipoperguntasID == 3) {
                                questoes[value.id_pergunta]["tipo"] = "checkbox";
                            }
                            questoes[value.id_pergunta]["ID"] = value.id_pergunta;

                        }
                    });

                    Object.values(result).forEach(value => {
                        questoes[value.id_pergunta]["alternativas"].push(
                            {
                                nome: value.alternativa,
                                ID: value.alternativaID
                            });
                    });

                    for (var i = 0; i < questoes.length; i++) {
                        if (questoes[i] == null) {
                            questoes.splice(i, 1);
                            i--;
                        }
                    }

                    res.status(200).send(questoes);
                    conn.release();
                }
            });
    });

};

exports.bovCorQuestions = function (req, res) {

    db.getConnection(function (err, conn) {
        conn.query("select a.alternativa, pa.alternativaID, p.pergunta, p.id_pergunta, p.tipoperguntasID from perguntas_alternativas as pa inner join perguntas as p on p.id_pergunta = pa.perguntaID inner join alternativas as a on a.id_alternativa = pa.alternativaID where p.tipoproducaoID = 1;"
            , (error, result) => {
                if (result.length === 0) {
                    res.status(204).send({msg: "Não a registros"});
                    conn.release();
                } else {

                    var questoes = [];

                    // alternativa
                    // pergunta
                    // id_pergunta

                    // Adiciona todas as perguntas;
                    Object.values(result).forEach(value => {
                        if (questoes[value.id_pergunta] == null) {
                            questoes[value.id_pergunta] = {};
                            questoes[value.id_pergunta]["pergunta"] = value.pergunta;
                            questoes[value.id_pergunta]["alternativas"] = [];
                            // questoes[value.id_pergunta]["id"] = value.id_pergunta;
                            if (value.tipoperguntasID == 1) {
                                questoes[value.id_pergunta]["tipo"] = "radio";
                            } else if (value.tipoperguntasID == 2) {
                                questoes[value.id_pergunta]["tipo"] = "input";
                            } else if (value.tipoperguntasID == 3) {
                                questoes[value.id_pergunta]["tipo"] = "checkbox";
                            }
                            questoes[value.id_pergunta]["ID"] = value.id_pergunta;

                        }
                    });

                    Object.values(result).forEach(value => {
                        questoes[value.id_pergunta]["alternativas"].push(
                            {
                                nome: value.alternativa,
                                ID: value.alternativaID
                            });
                    });

                    for (var i = 0; i < questoes.length; i++) {
                        if (questoes[i] == null) {
                            questoes.splice(i, 1);
                            i--;
                        }
                    }

                    res.status(200).send(questoes);
                    conn.release();
                }
            });
    });

};

exports.bovLeiQuestions = function (req, res) {

    db.getConnection(function (err, conn) {
        conn.query("select a.alternativa, pa.alternativaID, p.pergunta, p.id_pergunta, p.tipoperguntasID from perguntas_alternativas as pa inner join perguntas as p on p.id_pergunta = pa.perguntaID inner join alternativas as a on a.id_alternativa = pa.alternativaID where p.tipoproducaoID = 2;"
            , (error, result) => {
                if (result.length === 0) {
                    res.status(204).send({msg: "Não a registros"});
                    conn.release();
                } else {

                    var questoes = [];

                    // alternativa
                    // pergunta
                    // id_pergunta

                    // Adiciona todas as perguntas;
                    Object.values(result).forEach(value => {
                        if (questoes[value.id_pergunta] == null) {
                            questoes[value.id_pergunta] = {};
                            questoes[value.id_pergunta]["pergunta"] = value.pergunta;
                            questoes[value.id_pergunta]["alternativas"] = [];
                            // questoes[value.id_pergunta]["id"] = value.id_pergunta;
                            if (value.tipoperguntasID == 1) {
                                questoes[value.id_pergunta]["tipo"] = "radio";
                            } else if (value.tipoperguntasID == 2) {
                                questoes[value.id_pergunta]["tipo"] = "input";
                            } else if (value.tipoperguntasID == 3) {
                                questoes[value.id_pergunta]["tipo"] = "checkbox";
                            }
                            questoes[value.id_pergunta]["ID"] = value.id_pergunta;

                        }
                    });

                    Object.values(result).forEach(value => {
                        questoes[value.id_pergunta]["alternativas"].push(
                            {
                                nome: value.alternativa,
                                ID: value.alternativaID
                            });
                    });

                    for (var i = 0; i < questoes.length; i++) {
                        if (questoes[i] == null) {
                            questoes.splice(i, 1);
                            i--;
                        }
                    }

                    res.status(200).send(questoes);
                    conn.release();
                }
            });
    });

};

exports.avePosQuestions = function (req, res) {

    db.getConnection(function (err, conn) {
        conn.query("select a.alternativa, pa.alternativaID, p.pergunta, p.id_pergunta, p.tipoperguntasID from perguntas_alternativas as pa inner join perguntas as p on p.id_pergunta = pa.perguntaID inner join alternativas as a on a.id_alternativa = pa.alternativaID where p.tipoproducaoID = 3;"
            , (error, result) => {
                if (result.length === 0) {
                    res.status(204).send({msg: "Não a registros"});
                    conn.release();
                } else {

                    var questoes = [];

                    // alternativa
                    // pergunta
                    // id_pergunta

                    // Adiciona todas as perguntas;
                    Object.values(result).forEach(value => {
                        if (questoes[value.id_pergunta] == null) {
                            questoes[value.id_pergunta] = {};
                            questoes[value.id_pergunta]["pergunta"] = value.pergunta;
                            questoes[value.id_pergunta]["alternativas"] = [];
                            // questoes[value.id_pergunta]["id"] = value.id_pergunta;
                            if (value.tipoperguntasID == 1) {
                                questoes[value.id_pergunta]["tipo"] = "radio";
                            } else if (value.tipoperguntasID == 2) {
                                questoes[value.id_pergunta]["tipo"] = "input";
                            } else if (value.tipoperguntasID == 3) {
                                questoes[value.id_pergunta]["tipo"] = "checkbox";
                            }
                            questoes[value.id_pergunta]["ID"] = value.id_pergunta;

                        }
                    });

                    Object.values(result).forEach(value => {
                        questoes[value.id_pergunta]["alternativas"].push(
                            {
                                nome: value.alternativa,
                                ID: value.alternativaID
                            });
                    });

                    for (var i = 0; i < questoes.length; i++) {
                        if (questoes[i] == null) {
                            questoes.splice(i, 1);
                            i--;
                        }
                    }

                    res.status(200).send(questoes);
                    conn.release();
                }
            });
    });

};

exports.saveForm = async function (req, res) {

    id = req.body["idProprietario"];
    respostas = req.body["respostas"];

    data = new Map();

    db.getConnection(function (err, conn) {

        let promise = new Promise((resolve, reject) => {
            conn.query("insert into formularios (data_formulario,proprietarioID) values (now(),?);", [id]);
            conn.query("select id_formulario as idForm from formularios where proprietarioID = ? order by data_formulario DESC LIMIT 1;",
                [id],
                function (error, result) {
                    resolve(result[0]["idForm"]);
                });
        });

        promise.then(idform => {
           data.set("idForm",idform);
           new Promise(resolve => {
              for (var resposta of respostas){
                  conn.query(
                      "insert into respostas (disertativa_resposta, alternativaID, perguntasID,formID) values (?,?,?,?)",
                      [null, resposta["idAlternativa"], resposta["idPergunta"], data.get("idForm")]
                  );
              }

              conn.query("select id_resposta as id from respostas where formID = ?;", [data.get("idForm")], (error, result) => {
                   for (var resposta of result) {
                       conn.query(
                           "insert into formularios_respostas (formularioID, respostasID) value (?,?)",
                           [data.get("idForm"), resposta["id"]]
                       );
                   }
              });
              resolve();
           }).then(() => {
               res.status(200).send({"success":true});
           });
        });

        conn.release();
    });

};

exports.getProgress = function (req, res) {

    id = req.body['idProp'];
    data = new Map();

    db.getConnection(function (err, conn) {

        let promise = new Promise(
            function (resolve,reject) {
                conn.query("select id_formulario as idForm from formularios where proprietarioID = ? order by data_formulario DESC LIMIT 1;",
                    [id],
                    function (error,result) {
                        if(error){
                            reject("Internal Error");
                        }
                        if(result.length === 0){
                            reject("Proprietario Not Found");
                        }else {
                            resolve(result[0]["idForm"])
                        }
                    });
            }
        );


        promise.then(idForm => {
            data.set("idForm", idForm);

            new Promise(
                function (resolve) {
                    conn.query("select count(id_resposta) as total, tipoproducaoID as tipo from respostas as r inner join formularios as f on f.id_formulario = r.formID inner join perguntas as p on p.id_pergunta = r.perguntasID where proprietarioID = ? and p.tipoperguntasID = 3 and id_formulario = ?;",
                        [id, data.get("idForm")],
                        function (error, result) {
                            resolve(result[0])
                        });
                }
            ).then(result => {
                data.set("tipo",result['tipo']);
                data.set("total_respondido",result['total']);

                new Promise(
                    function (resolve) {
                        conn.query("select count(id_pergunta) as total from perguntas as p inner join perguntas_alternativas as pl on pl.perguntaID = p.id_pergunta where tipoperguntasID = 3 and tipoproducaoID = ?;",
                            [data.get("tipo")],
                            function (error, result) {
                                    resolve(result[0]['total'])
                            });
                    }
                ).then( (value) => {
                    data.set("total_questoes",value);
                    res.status(200).send({
                                    "respondido": data.get("total_respondido"),
                                    "total": data.get("total_questoes"),
                                    "porcentagem": (data.get("total_respondido") / data.get("total_questoes")) * 100
                                })
                });
            });
        });
        promise.catch(reason => {
            res.status(204).send(reason);
        });

        conn.release();
    });
};
