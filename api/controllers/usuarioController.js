const db = require("../db");
const crypto = require("crypto");

exports.login = function(req, res){

    email = req.body["email"];
    senha = returnHash(req.body["senha"]);


    db.getConnection(function (err,conn) {
        conn.query("select * from proprietarios where email_proprietario = ? and senha_proprietario = ?", [email,senha], (error, result) => {
            if(result.length === 0){
                res.status(204).send({msg:"Proprietario nao encontrado"});
                conn.release();
            }else{
                res.status(200).send(result[0]);
                conn.release();
            }
        });
    });

};

exports.cadastro = function (req, res) {

    nome = req.body["nome"];
    telefone = req.body["telefone"];
    celular = req.body["celular"];
    email = req.body["email"];
    senha = returnHash(req.body["senha"]);


    db.getConnection(function (err,conn) {
        conn.query("insert into proprietarios (nome_proprietario, email_proprietario, senha_proprietario, telefone_fixo, celular) values (?,?,?,?,?)", [nome, email, senha, telefone, celular], (error, result) => {
            if(error){
                console.log(error);
                res.status(400).send({msg:"Nao foi possivel inserir registro"});
                conn.release();
            }else{
                res.status(200).send({msg:"Registro inserido com sucesso"});
                conn.release();
            }
        });
    });

};

exports.editar = function (req, res){
    
    id = req.params.id;
    nome = req.body["nome"];
    telefone = req.body["telefone"];
    celular = req.body["celular"];
    
    db.getConnection(function (err, conn){
        conn.query("update proprietarios set nome_proprietario = ?, telefone_fixo = ?, celular = ? WHERE id_proprietario = ? ;", [nome, telefone, celular, id], (error, result) => {
            if(error){
                console.log(error);
                res.status(400).send({ msg:"Não foi possivel editar" });
                conn.release();
            }else{
                res.status(200).send({ msg:"Registro editado com sucesso" });
                conn.release();
            }
        });
    });

};

function returnHash(senha) {
    let salt = "... salada saladinha bem temperadinha com sal pimenta fogo foguinho ...";
    senha += senha.split('').reverse().join();
    senha = crypto.createHash("ripemd160WithRSA").update(salt + senha).digest('hex');
    return crypto.createHash("whirlpool").update(senha).digest('hex');
}

