const express = require("express");
const router = express.Router();

const controller = require("../controllers/usuarioController");

router.post("/login",controller.login);
router.post("/cadastro",controller.cadastro);
router.put("/editar/:id", controller.editar);

module.exports = router;
