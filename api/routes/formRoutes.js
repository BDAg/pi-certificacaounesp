const express = require("express");
const router = express.Router();

const controller = require("../controllers/formController");

router.post("/info/lastform",controller.lastform);
router.get("/questions/general",controller.generalQuestions);
router.get("/questions/bovino_corte",controller.bovCorQuestions);
router.get("/questions/bovino_leite",controller.bovLeiQuestions);
router.get("/questions/ave_postura",controller.avePosQuestions);
router.post("/save",controller.saveForm);
router.post("/info/progress", controller.getProgress);


module.exports = router;
