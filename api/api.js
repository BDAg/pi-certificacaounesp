const express = require("express");
const bodyParser = require('body-parser');
// const cors = require("cors");
// Import Rotas;
const usuarioRotas = require("./routes/usuarioRoutes");
const formRotas = require("./routes/formRoutes");

const api = express();

// Recursos;
// app.use(cors());
api.use(bodyParser.json()); // support json encoded bodies
api.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Rotas;
api.use("/usuario",usuarioRotas);
api.use("/form", formRotas);


api.listen(3000, log => {
    console.log("Api rodando")
});


