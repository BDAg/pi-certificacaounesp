# CePS - Certificações em Pecuária Sustentável
Esse projeto tem como objetivo criar um aplicativo para recomendar o produtor quando pedir a certificação.

# Documentação de Gestão

- [Equipe](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Equipe)
- [MVP - Minimal Viable Product](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/MVP)
- [Cronograma](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Cronograma)
- [Project Charter](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Project-Charter)
- [Mapa de Conhecimento](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Mapa-de-Conhecimento)
- [Mapa de Definição Tecnológica](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Mapa-de-Definição-Tecnológica)
- [Matriz de Habilidades](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Matriz-de-Habilidades)
- [Lista de funcionalidades](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Lista-de-Funcionalidade)

# Documentação da Aplicação

- [Modelo de Entidade Relacional(MER)](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Modelo-de-Entidade-Relacional-(MER))
- [Modelagem da API](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Modelagem-da-API)
- [Mockup das Telas](https://gitlab.com/BDAg/pi-certificacaounesp/wikis/Mockup-das-Telas)